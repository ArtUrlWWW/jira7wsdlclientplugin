package arthur.khusnutdinov.jira.plugins.lotus.servlet;

import arthur.khusnutdinov.jira.plugins.lotus.bridge.wsdl.GlobalWeather;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class MyServlet extends HttpServlet {

    private static final Logger log = LoggerFactory.getLogger(MyServlet.class);

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        resp.getWriter().write("<html><body>");

//        System.setProperty("javax.xml.stream.XMLOutputFactory", "com.ctc.wstx.stax.WstxInputFactory");
        GlobalWeather g = new GlobalWeather();
        String citiesByCountry = g.getGlobalWeatherSoap().getCitiesByCountry("US");
        System.out.println(citiesByCountry);
        resp.getWriter().write(citiesByCountry);
        resp.getWriter().write("</body></html>");
    }

}
