package ut.arthur.khusnutdinov.jira.plugins.lotus.bridge;

import org.junit.Test;
import arthur.khusnutdinov.jira.plugins.lotus.bridge.api.MyPluginComponent;
import arthur.khusnutdinov.jira.plugins.lotus.bridge.impl.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}